module.exports = function (grunt) {
  'use strict';

  var _ = require('lodash');

  // Load all npm dependencies
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    lib: grunt.file.readJSON('bower.json'),
    allScripts: ['libs/require', 'scripts/application'],


    //
    // Configure projects validators
    // -----------------------------------------

    jshint: {
      dev: {
        options: {
          globals: {
            "console": true,
            "require": true,
            "define": true,
            "_": true,
            "$": true,
            "Backbone": true
          }
        },
        src: [
          'app/scripts/*.js',
          'app/widgets/**/*.js'
        ]
      }
    },

    csslint: {
      dev: {
        options: {
          "adjoining-classes": false,
          "box-sizing": false,
          "box-model": false,
          "compatible-vendor-prefixes": false,
          "floats": false,
          "font-sizes": false,
          "gradients": false,
          "important": false,
          "known-properties": false,
          "outline-none": false,
          "qualified-headings": false,
          "regex-selectors": false,
          "shorthand": false,
          "text-indent": false,
          "unique-headings": false,
          "universal-selector": false,
          "unqualified-attributes": false
        },
        src: ['app/styles/*.css']
      }
    },


    //
    // Configure watching project's development
    // -----------------------------------------

    express: {
      dev: {
        options: {
          port: 9000,
          hostname: 'localhost',
          bases: ['app'],
          livereload: true
        }
      }
    },

    open: {
      dev: {
        url: 'http://localhost:<%= express.dev.options.port %>'
      }
    },

    watch: {
      less: {
        files: 'app/styles/*.less',
        tasks: ['less:dev']
      },
      css: {
        files: 'app/styles/application.css',
        tasks: ['csslint:dev'],
        options: {
          livereload: true
        }
      },
      js: {
        files: [
          'app/scripts/*.js',
          'app/widgets/**/*.js'
        ],
        tasks: ['jshint:dev'],
        options: {
          livereload: true
        }
      }
    },


    //
    // Configure installing project's files
    // -----------------------------------------

    bower: {
      install: {
        options: {
          copy: false
        }
      }
    },

    clean: {
      dev: ['app/libs/*'],
      dist: ['dist/*']
    },

    copy: {
      dist: {
        files: [
          {expand: true, src: ['app/fonts/*'], dest: 'dist/fonts/'}
        ]
      }
    },


    //
    // Configure creating documentation files
    // -----------------------------------------

    yuidoc: {
      compile: {
        name: "<%= pkg.name %>",
        description: "<%= pkg.description %>",
        version: "<%= pkg.version %>",
        url: "<%= pkg.homepage %>",
        options: {
          paths: [ "app/scripts", "app/widgets" ],
          outdir: "docs"
        }
      }
    },


    //
    // Configure building project's files
    // -----------------------------------------

    htmlmin: {
      dist: {
        options: {
          removeComments: true,
          removeCommentsFromCDATA: true,
          collapseWhitespace: true,
          collapseBooleanAttributes: true,
          removeAttributeQuotes: false,
          removeRedundantAttributes: false,
          useShortDoctype: true,
          removeEmptyAttributes: false,
          removeOptionalTags: false
        },
        files: [{
          expand: true,
          cwd: 'app',
          src: '*.html',
          dest: 'dist'
        }]
      }
    },

    less: {
      dev: {
        options: {
          paths: ['app/styles']
        },
        files: {
          'app/styles/application.css': 'app/styles/application.less'
        }
      },
      dist: {
        options: {
          paths: ['app/styles'],
          yuicompress: true
        },
        files: {
          'dist/styles/application.css': 'app/styles/application.less'
        }
      }
    },

    replace: {
      dist: {
        options: {
          patterns: [
            {
              match: /<script data-main="scripts\/application" src="libs\/require.js">/,
              replacement: '<script src="scripts/application.js">'
            }
          ]
        },
        files: [
         {expand: true, flatten: true, src: ['dist/index.html'], dest: 'dist/'}
        ]
      }
    },

    requirejs: {
      dist: {
        options: {
          baseUrl: 'app',
          mainConfigFile: 'app/scripts/application.js',
          findNestedDependencies: true,
          removeCombined: false,
          optimize: 'uglify',
          include: "<%= allScripts %>",
          out: 'dist/scripts/application.js'
        }
      }
    }

  });


  //
  // Register helper tasks
  // -----------------------------------------

  // Install bower components inside our project
  grunt.registerTask('bower_copy', 'Copy bower components', function() {
    var bowerJSON = grunt.config.get('lib'), status = true;
    _.each(bowerJSON.copy, function(dist, src) {
      src = 'bower_components/' + src;
      dist = 'app/' + dist;
      if (grunt.file.isFile(src)) {
        grunt.file.copy(src, dist);
        grunt.log.writeln('Copy success: ' + dist);
      } else {
        grunt.log.error('File does\'t exists: ' + src);
        status = false;
      }
    });
    return status;
  });

  // Add all widgets to require task
  grunt.registerTask('require_widgets', 'Collect all widgets', function() {
    var widgetsPaths = grunt.file.expand('app/widgets/**/main.js');
    var allScripts = grunt.config.get('allScripts');
    _.each(widgetsPaths, function(path) {
      allScripts.push(path.replace(/^app\//, '').replace(/.js$/, ''));
    });
    grunt.config.set('allScripts', allScripts);
  });


  //
  // Register all important tasks
  // -----------------------------------------

  // Prepare project on start on update
  grunt.registerTask('prepare', ['bower', 'bower_copy']);

  // I'm pretty out of habit to use this
  grunt.registerTask('lint', ['jshint', 'less:dev', 'csslint']);

  // Open server and listen to all changes
  grunt.registerTask('run', ['less:dev', 'express', 'open', 'watch']);

  // Time to deploy
  grunt.registerTask('build', ['lint', 'clean:dist', 'htmlmin:dist', 'less:dist', 'copy:dist', 'replace:dist', 'require_widgets', 'requirejs:dist']);

  // For now as default check javascripts
  grunt.registerTask('default', ['lint']);

};
