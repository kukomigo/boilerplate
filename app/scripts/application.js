requirejs.config({
  baseUrl: '.',
  paths: {
    sandbox: 'scripts/sandbox',
    jquery: 'libs/jquery',
    text: 'libs/require-text',
    underscore: 'libs/lodash',
    emitter: 'libs/eventemitter',
    backbone: 'libs/backbone',
    storage: 'libs/backbone.localstorage'
  },
  shim: {
    backbone: {
      deps: ['jquery', 'underscore'],
      exports: 'Backbone'
    },
    storage: {
      deps: ['backbone'],
      exports: 'Backbone'
    }
  }
});

// Start the main app logic.
requirejs([
  'scripts/widgets'
],

function(widgets) {

  // Execute main widgets.
  widgets.start({ channel: 'todos', options: { el: 'sample' } });

});
