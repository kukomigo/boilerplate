define([
  'sandbox'
],

function(sandbox) {

  /**
   * Run specific widget.
   *
   * @private
   * @method execute
   * @param {Function} widget
   */
  function initWidget(channel, options) {
    require(["widgets/" + channel + "/main"], function(widget) {
      try {
        widget(options);
      } catch(e) {
        console.log(e);
      }
    });
  }

  /**
   * The App's widgets manager.
   *
   * @public
   * @class widgets
   * @constructor
   */
  var widgets = {};

  /**
   * The storage of all loaded widgets.
   *
   * @property channels
   * @type {Object}
   * @default {}
   */
  widgets.channels = {};

  /**
   * Automatically load a widget and initialize it.
   *
   * @method start
   * @param {String|Object} arguments
   */
  widgets.start = function() {
    var channel, options, key;
    var args = Array.prototype.slice.call(arguments);

    for (key in args) {
      if (args.hasOwnProperty(key)) {

        if (typeof args[key] === "object") {
          channel = args[key].channel;
          options = args[key].options || { el: channel };
        } else if (typeof args[key] === "string") {
          channel = args[key];
          options = { el: args[key] };
        }

        if (channel && this.channels[channel] === undefined) {
          this.channels[channel] = options;
          initWidget(channel, options);
        }

      }
    }
  };

  return widgets;

});
