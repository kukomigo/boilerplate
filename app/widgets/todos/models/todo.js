define([
  'sandbox'
],

function(sandbox) {
  var TodoModel = sandbox.Model({

    // Default attributes for the todo.
    defaults: {
      title: '',
      description: '',
      completed: 0
    }

  });

  return TodoModel;
});
