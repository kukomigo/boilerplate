define([
  'sandbox',
  'text!widgets/todos/templates/todo.html'
],

function(sandbox, todoTemplate) {
  var TodoView = sandbox.View({

    // Our template for the todo element.
    template: sandbox.template(todoTemplate),

    // Each newly created element will be put inside this tag.
    tagName: 'li',

    // The DOM events specific to an item.
    events: {
      'click': 'select'
    },

    // Listens to changes inside our model.
    initialize: function () {
      this.model.bind('change', this.render, this);
      this.model.bind('destroy', this.remove, this);
      this.model.bind('close', this.close, this);
    },

    // Render the contents of the todo item.
    render: function() {
      var attributes = this.model.attributes;
      this.$el.attr('data-cid', this.model.cid);
      this.$el.html(this.template(attributes));
      return this;
    },

    // Perform select action.
    select: function() {
      this.$el.addClass('active');
      sandbox.emit('todo:selected', this.model);
    },

    // Perform deselect action
    close: function() {
      this.$el.removeClass('active');
    }

  });

  return TodoView;
});
