define([
  'sandbox',
  'widgets/todos/collections/todos',
  'widgets/todos/views/todo',
  'text!widgets/todos/templates/box.html'
],

function(sandbox, todos, TodoView, boxTemplate) {
  var WidgetView = sandbox.View({

    template: sandbox.template(boxTemplate),

    initialize: function () {
      todos.add([
        { title: 'test 1' },
        { title: 'test 2', test: 'tested' }
      ]);
      this.render();
      sandbox.on('todo:selected', this.deselectTodo, this);
    },

    render: function() {
      this.$el.html(this.template);
      todos.each(this.addTodo, this);
    },

    addTodo: function(todo) {
      var view = new TodoView({
        model: todo
      });
      this.$el.find('ul.elems').append(view.render().el);
    },

    deselectTodo: function(model) {
      var elements = this.$el.find('.elems li.active');
      elements.each(function() {
        var cid = this.getAttribute('data-cid');
        if (model.cid !== cid) {
          todos.get(cid).trigger('close');
        }
      });
    }

  });

  return WidgetView;
});
