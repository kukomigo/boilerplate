define([
  'sandbox',
  'widgets/todos/models/todo'
],

function(sandbox, TodoModel) {
  var TodosCollection = sandbox.Collection({

    // Reference to this collection's model.
    model: TodoModel

  });

  return new TodosCollection();
});
