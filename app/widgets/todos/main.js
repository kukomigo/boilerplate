define([
  'sandbox',
  'widgets/todos/views/widget'
],

function(sandbox, WidgetView) {
  return function(options) {
    new WidgetView({
      el: sandbox.widget(options.el)
    });
  };
});
